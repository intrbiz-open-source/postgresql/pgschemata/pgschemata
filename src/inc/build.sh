#
# Build functions used to compile schema scripts
#
#
# Flags used during build:
# BUILD_MODE=create|update

#
# Say a message
#
function say {
  echo "SELECT _admin.say('$1');"
}

#
# Say a message
#
function error {
  echo "SELECT _admin.error('$1');"
}

#
# Start this
#
function begin {
  if [ "$USE_PSQL" == "yes" ]; then
    echo "\set ON_ERROR_ROLLBACK 1"
    echo "\set ON_ERROR_STOP true"
    echo "\set AUTOCOMMIT off"
  fi
  echo "BEGIN;"
  echo "SET check_function_bodies = false;"
  IN_TRANS=yes;
}

#
# Commit this
#
function commit {
  echo "COMMIT;"
  if [ "$USE_PSQL" == "yes" ]; then
    echo "\set AUTOCOMMIT on"
  fi
  IN_TRANS=no;
}

#
# Apply a migration
#
function migration {
  if [ -e "./migrations/$1.sql" ]; then
      if [ "$BUILD_MODE" == "create" ]; then
      echo "SELECT _admin.skip_migration('${1}');"
      else
      echo "SELECT _admin.apply_migration('${1}', \$migration\$"
      cat "./migrations/$1.sql"
      echo "\$migration\$);"
      fi
  elif [ -e "./migrations/$1.sh" ]; then
      if [ "$BUILD_MODE" == "create" ]; then
      echo "SELECT _admin.skip_migration('${1}');"
      else
      echo "SELECT _admin.apply_migration('${1}', \$migration\$"
      # some includes gate on the build mode, so we flip it during migration building
      PREV_BUILD_MODE=$BUILD_MODE
      export BUILD_MODE="create"
      source "./migrations/$1.sh"
      export BUILD_MODE=$PREV_BUILD_MODE
      echo "\$migration\$);"
      fi
  else
      error "No such migration ${1}"
  fi
}

#
# Skip a migration
#
function skip_migration {
  echo "SELECT _admin.skip_migration('$1');"
}

#
# Apply some seed data
#
function seed {
  if [ -e "./seeds/$1.sql" ]; then
    echo "SELECT _admin.apply_seed('$1', \$seed\$"
    cat "./seeds/$1.sql"
    echo "\$seed\$);"
  else
    error "No such seed ${1}"
  fi
}

#
# Skip a migration
#
function skip_seed {
  echo "SELECT _admin.skip_seed('$1');"
}

#
# Manually mark the schema version, this should be called just before commit
#
function version {
  if [ -z "$SET_VERSION" ]; then
    SET_VERSION="yes"
    if [ "$ALLOW_VERSION_UPDATE" = "yes" ]; then
      echo "SELECT _admin.mark_version('$1', TRUE);"
    else
      echo "SELECT _admin.mark_version('$1', FALSE);"
    fi
  fi
}

#
# Guess the version from the SCM system
#
function auto_version {
  if [ -z "$SET_VERSION" ]; then
    # try to generate a version number
    VERSION=$(git describe --tags 2>/dev/null)
    if [ "$?" -ne "0" ]; then
      VERSION=$(git log -1 --format="%H" 2>/dev/null)
    fi
    if [ "$?" -ne "0" ]; then
      VERSION=$(date '+%s')
    fi
    version "$VERSION"
  fi
}

#
# Include a SQL script, usage:
# include <script_path>
#
function include {
  if [ "$IN_MIGRATION" = "yes" ]; then
    if [ -e "../$1" ]; then
      cat "../$1" | tr -cd '\11\12\15\40-\176'
      echo ""
    else
      echo "SELECT _admin.error('Failed to include: `pwd`/../$1 (IN_MIGRATION), file not found');"
    fi
  else
    if [ -e "./$1" ]; then
      cat "./$1" | tr -cd '\11\12\15\40-\176'
      echo ""
    else
      echo "SELECT _admin.error('Failed to include: `pwd`/$1, file not found');"
    fi
  fi
}

function include_view {
  if [ "$BUILD_MODE" == "create" ]; then
    say "Loading view $1"
    include "views/$1.sql"
  fi
}

function include_table {
  if [ "$BUILD_MODE" == "create" ]; then
    say "Loading table $1"
    include "tables/$1.sql"
  fi
}

function include_type {
  if [ "$BUILD_MODE" == "create" ]; then
    say "Loading type $1"
    include "types/$1.sql"
  fi
}

function include_enum {
  if [ "$BUILD_MODE" == "create" ]; then
    say "Loading enum $1"
    include "enums/$1.sql"
  fi
}

function include_trigger {
  if [ "$BUILD_MODE" == "create" ]; then
    say "Loading trigger $1"
    include "triggers/$1.sql"
  fi
}

function include_aggregate {
  if [ "$BUILD_MODE" == "create" ]; then
    say "Loading aggregate $1"
    include "aggregates/$1.sql"
  fi
}


function include_function {
    say "Loading function $1"
    include "functions/$1.sql"
}

function include_test {
    say "Loading test $1"
    include "tests/$1.sql"
}

function include_role {
  say "Loading role $1"
  if [ -e "./roles/$1.sql" ]; then
    echo "SELECT _admin.apply_migration('${1}', \$role\$"
    cat "./roles/$1.sql"
    echo "\$role\$);"
  elif [ -e "./roles/$1.sh" ]; then
    echo "SELECT _admin.apply_migration('${1}', \$role\$"
    source "./roles/$1.sh"
    echo "\$role\$);"
  else
    error "No such role ${1}"
  fi
}

#
# Execute tests as part of the deployment, failing the deployment should 
# a test fail
#
function run_tests {
  if [ "$SKIP_TESTS" != "yes" ]; then
    if [ "$INSTALL_PGTAP" != "no" ]; then
      echo "CREATE SCHEMA IF NOT EXISTS tap; CREATE EXTENSION IF NOT EXISTS pgtap SCHEMA tap;"
    fi
    if [ "$2" ]; then
      echo "SET search_path to tap, public;"
      echo "SELECT _admin.run_tests('$1'::name, '$2');"
    else
      echo "SET search_path to tap, public;"
      echo "SELECT _admin.run_tests('$1'::name, '^pgtest');"
    fi
  fi
}

function include_schema {
  echo "CREATE SCHEMA IF NOT EXISTS \"$1\";"
  pushd $1 >/dev/null
  source ./schema.sh
  popd >/dev/null
}
