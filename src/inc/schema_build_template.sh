# Schema build script for $SCHEMA

say "Installing $SCHEMA"

#
# Build commands can be specified in any order as needed
#

# Tables
#include_table 'name'

# Types
#include_type 'name'
#include_enum 'name'

# Views
#include_view 'name'

# Seeds - data seeds which need to be applied only once
#seed 'name'

# Migrations - version migrations (adding a column, table etc) which need to be applied only once
#migration 'name'

# Functions
#include_function 'name'
#include_aggregate 'name'

# Triggers
#include_trigger 'name'

# Tests
#include_test 'name'
