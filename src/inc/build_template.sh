# Database schema build for $NAME
#
# The database schema can be built with: ./pgSchemata build create or ./pgSchemata build update
#

say "Installing $NAME"

# Schemas
include_schema public

# Run Tests
run_tests public '^test'
