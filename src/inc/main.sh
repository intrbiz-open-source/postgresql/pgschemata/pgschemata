#
# pgSchemata main
#

function usage {
    cat <<EOF
pgSchemata v1.0.0

usage: ${SCRIPT_NAME} <command> <args>

Commands:
  init <name>  - Initialise a pgSchemata repository, in ./<name>
  add <name>   - Add a new schema to this (./) repository
  build admin  - Build the admin schema installation script
  build create - Build the schema in create mode, for installing into a clean database
  build update - Build the schema in update mode, for updating an existing database
  about        - Display README
EOF
    exit 1
}

function add {
    SCHEMA="$1"
    mkdir "./$SCHEMA"
    for x in enums functions migrations seeds tables tests triggers types; do
        mkdir "./$SCHEMA/$x"
        touch "./$SCHEMA/$x/.gitkeep"
    done
    schema_build_template > "./$SCHEMA/schema.sh"
}

function init {
    NAME="$1"
    if [ -z "$NAME" ]; then
        usage
    else
        mkdir "./$NAME"
        cp $SCRIPT_PATH "./$NAME/pgSchemata.sh"
        readme_text > "./$NAME/README.md"
        pushd "./$NAME" >/dev/null
        add 'public'
        build_template > "schemas.sh"
        popd >/dev/null
    fi
}

function build_admin {
    begin
    admin_schema
    commit
}

function build_create {
    begin
    
    BUILD_MODE=create
    source ./schemas.sh
    
    auto_version
    commit
}

function build_update {
    begin
    
    BUILD_MODE=update
    source ./schemas.sh
    
    auto_version
    commit
}

function build {
    case "$1" in
    "admin")
        build_admin
    ;;
    "create")
        build_create
    ;;
    "update")
        build_update
    ;;
    *)
        usage
    ;;
    esac
}

# Main
SCRIPT_PATH=$0
SCRIPT_NAME=$(basename $0)
SCRIPT_DIR=$(dirname $0)
if [ "$SCRIPT_NAME" == "pgSchemata.sh" ]; then
    case "$1" in
    "init")
        init $2
    ;;
    "add")
        add $2
    ;;
    "build")
        pushd $SCRIPT_DIR >/dev/null
        build $2
        popd >/dev/null
    ;;
    "about")
        readme_text
    ;;
    *)
        usage
    ;;
    esac
fi
