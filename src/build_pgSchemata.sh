#!/bin/bash
DIR=$(dirname $0)
pushd $DIR >/dev/null

OUT="./pgSchemata.sh"

# build the admin schema
pushd admin >/dev/null
./assemble_schema.sh > admin.sql
popd >/dev/null

# assemble pgSchemata
echo '#!/bin/bash' > $OUT
echo ""  >> $OUT

# include build functions
cat ./inc/build.sh >> $OUT
echo ""  >> $OUT

# bake in the admin schema
echo "function admin_schema {"  >> $OUT
echo "cat <<'EOF'"  >> $OUT
cat ./admin/admin.sql  >> $OUT
echo "EOF"  >> $OUT
echo "}"  >> $OUT
echo ""  >> $OUT

# bake in the readme
echo "function readme_text {"  >> $OUT
echo "cat <<'EOF'"  >> $OUT
cat ./inc/readme.md  >> $OUT
echo "EOF"  >> $OUT
echo "}"  >> $OUT
echo ""  >> $OUT

# bake in the schema build script template
echo "function schema_build_template {"  >> $OUT
echo "cat <<EOF"  >> $OUT
cat ./inc/schema_build_template.sh  >> $OUT
echo "EOF"  >> $OUT
echo "}"  >> $OUT
echo ""  >> $OUT

# bake in the build script template
echo "function build_template {"  >> $OUT
echo "cat <<EOF"  >> $OUT
cat ./inc/build_template.sh  >> $OUT
echo "EOF"  >> $OUT
echo "}"  >> $OUT
echo ""  >> $OUT

# include script main
cat ./inc/main.sh >> $OUT
echo ""  >> $OUT

# script permissions
chmod +x $OUT

# clean up
rm admin/admin.sql
mv $OUT ../$OUT

popd >/dev/null
