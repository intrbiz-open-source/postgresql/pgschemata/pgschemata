CREATE OR REPLACE FUNCTION _admin._apply(p_type TEXT, p_name TEXT, p_sql TEXT)
RETURNS VOID LANGUAGE plpgsql AS $$
DECLARE
  v_migration _admin.migrations;
BEGIN
  -- Track the migration
  PERFORM _admin._track(p_name);
  -- Has the migration already been run?
  SELECT * INTO v_migration FROM _admin.migrations WHERE "name" = p_name;
  IF FOUND THEN
    -- The migration has already run
    RAISE NOTICE 'Ignoring migration % since it was % on %', 
      p_name, 
      (CASE WHEN v_migration.applied THEN 'applied' WHEN v_migration.skipped THEN 'skipped' ELSE 'unknown' END), 
      v_migration.applied_on::TEXT;
  ELSE
    -- We can execute the migration
    INSERT INTO _admin.migrations ("name", "type", applied, skipped, deployed_on, applied_on, applied_by) VALUES (p_name, p_type, TRUE, FALSE, now(), clock_timestamp(), current_user);
    EXECUTE p_sql;
  END IF;
END;
$$;
