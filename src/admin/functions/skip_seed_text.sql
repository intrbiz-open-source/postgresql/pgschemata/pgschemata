CREATE OR REPLACE FUNCTION _admin.skip_seed(p_name TEXT)
RETURNS VOID LANGUAGE sql AS $$
  SELECT _admin._skip('seed', p_name);
$$;
