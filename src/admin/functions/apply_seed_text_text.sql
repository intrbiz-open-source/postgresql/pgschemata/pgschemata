CREATE OR REPLACE FUNCTION _admin.apply_seed(p_name TEXT, p_sql TEXT)
RETURNS VOID LANGUAGE sql AS $$
  SELECT _admin._apply('seed', p_name, p_sql);
$$;
