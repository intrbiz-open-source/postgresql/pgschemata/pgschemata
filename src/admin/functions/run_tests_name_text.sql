CREATE OR REPLACE FUNCTION _admin.run_tests(p_schema NAME, p_pattern TEXT)
RETURNS TEXT LANGUAGE plpgsql AS $$
DECLARE
  v_test_result TEXT;
  v_failed BOOLEAN;
  v_failed_count INTEGER;
  v_out TEXT;
BEGIN
    v_failed := FALSE;
    v_failed_count := 0;
    v_out := '';
    FOR v_test_result IN SELECT tap.runtests(p_schema, p_pattern) LOOP
      -- have we failed a test?
      IF (v_test_result ~ '^not') THEN
        v_failed := TRUE;
        v_failed_count := v_failed_count + 1;
      END IF;
      -- return the test result
      v_out := v_out || v_test_result || E'\n';
    END LOOP;
    -- did the tests fail?
    IF (v_failed) THEN
      RAISE EXCEPTION E'Deployment aborted due to % failed tests!\n%', v_failed_count, v_out;
    END IF;
    -- done
    RETURN v_out;
END;
$$;
