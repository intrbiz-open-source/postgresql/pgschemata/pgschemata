CREATE OR REPLACE FUNCTION _admin.deployed_version()
RETURNS TEXT LANGUAGE sql AS $$
  SELECT value::TEXT FROM _admin.metadata WHERE name = 'current_version';
$$;
