CREATE OR REPLACE FUNCTION _admin.mark_version(p_version TEXT, p_allow_update BOOLEAN DEFAULT FALSE)
RETURNS TEXT LANGUAGE plpgsql AS $$
DECLARE
BEGIN
  -- Ensure tracking exists
  PERFORM _admin._track(NULL);
  -- allow duplicate versions (IE: for local / dev envs)
  IF (p_allow_update) THEN
    DELETE FROM _admin.versions WHERE "version" = p_version;
  END IF;
  -- Log this version
  INSERT INTO _admin.versions ("version", deployed_on, deployed_by, migrations) 
    SELECT p_version, now(), current_user, (SELECT array_agg(qq.migration) FROM (SELECT q.* FROM (SELECT DISTINCT ON (migration) migration, applied FROM requested_migrations) q ORDER BY applied DESC) qq);
  -- Update the current version variable
  DELETE FROM _admin.metadata WHERE "name" = 'current_version';
  INSERT INTO _admin.metadata ("name", "value") VALUES ('current_version', p_version);
  -- Cleanup our temp table
  DELETE FROM requested_migrations;
  -- RETURN
  RETURN 'Version: ' || p_version;
END;
$$;
