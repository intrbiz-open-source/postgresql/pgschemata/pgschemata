CREATE OR REPLACE FUNCTION _admin.skip_migration(p_name TEXT)
RETURNS VOID LANGUAGE sql AS $$
  SELECT _admin._skip('migration', p_name);
$$;
