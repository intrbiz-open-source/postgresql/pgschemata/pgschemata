CREATE OR REPLACE FUNCTION _admin.find_functions(p_schema NAME, p_pattern TEXT)
RETURNS SETOF TEXT LANGUAGE plpgsql AS $$
BEGIN
  RETURN QUERY 
    SELECT quote_ident(n.nspname) || '.' || quote_ident(p.proname) || '(' || pg_get_function_identity_arguments(p.oid) || ')'
    FROM pg_proc p
    JOIN pg_namespace n ON (p.pronamespace = n.oid)
    WHERE n.nspname = p_schema AND (p.proname ~ p_pattern OR p_pattern IS NULL);
END;
$$;
