CREATE OR REPLACE FUNCTION _admin.drop_functions(p_schema NAME, p_pattern TEXT)
RETURNS VOID LANGUAGE plpgsql AS $$
DECLARE
  v_function_id TEXT;
BEGIN
  FOR v_function_id IN SELECT _admin.find_functions(p_schema, p_pattern) LOOP
    RAISE NOTICE 'Dropping function %', v_function_id;
    EXECUTE 'DROP FUNCTION IF EXISTS ' || v_function_id || ' CASCADE;';
  END LOOP;
END;
$$;
