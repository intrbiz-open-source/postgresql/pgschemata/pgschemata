CREATE OR REPLACE FUNCTION _admin.say(p_message TEXT)
RETURNS TEXT LANGUAGE plpgsql AS $$
BEGIN
  RAISE NOTICE '%', p_message;
  RETURN p_message;
END;
$$;
