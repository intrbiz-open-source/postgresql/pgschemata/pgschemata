CREATE OR REPLACE FUNCTION _admin.apply_migration(p_name TEXT, p_sql TEXT)
RETURNS VOID LANGUAGE sql AS $$
  SELECT _admin._apply('migration', p_name, p_sql);
$$;
