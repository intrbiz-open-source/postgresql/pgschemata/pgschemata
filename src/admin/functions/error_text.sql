CREATE OR REPLACE FUNCTION _admin.error(p_message TEXT)
RETURNS TEXT LANGUAGE plpgsql AS $$
BEGIN
  RAISE EXCEPTION '%', p_message;
  RETURN p_message;
END;
$$;
