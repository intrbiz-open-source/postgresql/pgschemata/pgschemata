CREATE OR REPLACE FUNCTION _admin._skip(p_type TEXT, p_name TEXT)
RETURNS VOID LANGUAGE plpgsql AS $$
DECLARE
  v_migration _admin.migrations;
BEGIN
  -- Track the migration
  PERFORM _admin._track(p_name);
  -- Skip skippy
  BEGIN
    INSERT INTO _admin.migrations ("name", "type", applied, skipped, deployed_on, applied_on, applied_by) VALUES (p_name, p_type, FALSE, TRUE, now(), clock_timestamp(), current_user);
  EXCEPTION
      WHEN unique_violation THEN
        /* nothing */
  END;
END;
$$;
