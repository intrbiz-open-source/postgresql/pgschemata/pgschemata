CREATE OR REPLACE FUNCTION _admin.run_tests(p_schema NAME)
RETURNS SETOF TEXT LANGUAGE sql AS $$
  SELECT * FROM _admin.run_tests($1, '^test');
$$;
