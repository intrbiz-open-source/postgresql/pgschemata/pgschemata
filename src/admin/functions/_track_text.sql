CREATE OR REPLACE FUNCTION _admin._track(p_name TEXT)
RETURNS VOID LANGUAGE plpgsql AS $$
BEGIN
  -- TEMP table to track migrations applied in this transaction
  SET client_min_messages TO WARNING;
  CREATE TEMP TABLE IF NOT EXISTS requested_migrations (
    migration  TEXT,
    applied    TIMESTAMP WITH TIME ZONE
  );
  SET client_min_messages TO NOTICE;
  IF (p_name IS NOT NULL) THEN
    INSERT INTO requested_migrations (migration) VALUES (p_name);
  END IF;
END;
$$;
