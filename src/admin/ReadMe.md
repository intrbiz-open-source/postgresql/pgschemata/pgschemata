# Admin Database Schema

This schema hold DBA tools which are used to load our schemas 
and generally useful for DBA type tasks.

This schema has a couple of helper functions for applying migrations 
and tracking versions.  Migrations move the database state from 
one to another, IE: they apply delta changes to the schema which 
can only happen once.

To apply a migration:

    SELECT _admin.apply_migration('<migration_name>', $migration$
      <put your SQL here>
    $migration$);

To load some seed data (using the semantics of a migration):

    SELECT _admin.apply_seed('<seed_name>', $seed$
      <put your SQL here>
    $seed$);

To explicitly skip a migration (to mark it as applied):

    SELECT _admin.skip_migration('<migration_name>');

To explicitly skip loading seed data (to mark it as applied):

    SELECT _admin.skip_seed('<seed_name>');

To mark the currently deployed database version:

    SELECT _admin.mark_version('<version>');

To get the currently deployed database version:

    SELECT _admin.deployed_version();
