#!/bin/bash

#
# Assemble the admin schema to a simple SQL script
#

echo "/* Install admin schema */"
echo "CREATE SCHEMA IF NOT EXISTS _admin;"

echo "/* Install tables */"
echo

# Install tables
cat "./tables/migrations.sql"
echo

cat "./tables/versions.sql"
echo

cat "./tables/metadata.sql"
echo


echo "/* Install functions */"
echo

cat "./functions/_track_text.sql"
echo

cat "./functions/_apply_text_text_text.sql"
echo

cat "./functions/_skip_text_text_text.sql"
echo

cat "./functions/apply_migration_text_text.sql"
echo

cat "./functions/apply_seed_text_text.sql"
echo

cat "./functions/skip_migration_text.sql"
echo

cat "./functions/skip_seed_text.sql"
echo

cat "./functions/deployed_version.sql"
echo

cat "./functions/mark_version_text_boolean.sql"
echo

cat "./functions/say_text.sql"
echo

cat "./functions/error_text.sql"
echo

cat "./functions/run_tests_name_text.sql"
echo

cat "./functions/run_tests_name.sql"
echo

cat "./functions/find_functions_name_text.sql"
echo

cat "./functions/drop_functions_name_text.sql"
echo
