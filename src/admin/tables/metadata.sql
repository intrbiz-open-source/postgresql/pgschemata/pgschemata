CREATE TABLE IF NOT EXISTS _admin.metadata
(
    "name"  TEXT NOT NULL,
    "value" TEXT NOT NULL,
    CONSTRAINT metadata_pk PRIMARY KEY ("name")
);
