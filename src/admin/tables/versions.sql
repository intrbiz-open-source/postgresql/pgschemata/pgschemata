CREATE TABLE IF NOT EXISTS _admin.versions
(
    "version"    TEXT NOT NULL,
    deployed_on  TIMESTAMP WITH TIME ZONE NOT NULL,
    deployed_by  TEXT NOT NULL,
    migrations   TEXT[],
    CONSTRAINT verions_pk PRIMARY KEY ("version")
);
