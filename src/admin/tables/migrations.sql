CREATE TABLE IF NOT EXISTS _admin.migrations
(
    "name"       TEXT NOT NULL,
    "type"       TEXT NOT NULL,
    applied      BOOLEAN NOT NULL,
    skipped      BOOLEAN NOT NULL,
    deployed_on  TIMESTAMP WITH TIME ZONE NOT NULL,
    applied_on   TIMESTAMP WITH TIME ZONE NOT NULL,
    applied_by   TEXT NOT NULL,
    CONSTRAINT migrations_pk PRIMARY KEY ("name")
);
